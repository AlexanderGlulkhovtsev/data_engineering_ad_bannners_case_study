# Data Engineering Case Study Challenge
> This project gets some insights  
in terms of gathering statistics  
and making recommendations  
about ads placement  
to maximize advertiser's revenue  
by analysing input data  
of advertisement bannners  
in mobile application ( tracked as app_id field )  
in country ( tracked as country code field )  
from advertiser ( tracked as advertiser_id )  
represented by   
`impression` ( ad displayed to users )  
and `click` ( user's reaction to the ad ) events  
recorded and stored in multiple files .  

## Installation:
### Prerequisites:
* installed `Java`(TM) SE Runtime Environment, at least:
  ```sh
  $ java -version
java version "1.8.0_201"
Java(TM) SE Runtime Environment (build 1.8.0_201-b09)
Java HotSpot(TM) 64-Bit Server VM (build 25.201-b09, mixed mode)
  ```
* installed `sbt` ( The interactive build tool ), e.g.:  
  ```sh
  $ cat ./project/build.properties
  sbt.version=1.2.8
  ```
  [detailed `sbt` installation instructions](https://www.scala-sbt.org/download.html)

### Configuring 
--toDo  

### installing dependencies  
automatically from `build.sbt` after:
```sh
$ sbt
```

### How to run:
--toDo  
to run :  
```sh
$ sbt
sbt:advertisement-recommendation-case-study> run
```
to discover clases with main method:  
```sh
sbt:advertisement-recommendation-case-study> show compile:discoveredMainClasses
[info] * Naive_Approach
```
to run with argiments passed to the only main method:  
`> run <args string with space separated values>`
to run specific main method:  
`> runMain Naive_Approach`
to run with argiments passed to the main method:  
`> runMain Naive_Approach <args string with space separated values>`

to [Run JAR Application With Command Line Arguments](https://www.baeldung.com/java-run-jar-with-arguments)
```sh
sbt:advertisement-recommendation-case-study> package
[info] Compiling 1 Scala source to ../target/scala-2.13/classes ...
[info] Done compiling.
[info] Packaging ../advertisement_recommendation_case_study/target/scala-2.13/advertisement-recommendation-case-study_2.13-0.1.jar ...
[info] Done packaging.
```
both:  
`$ java -jar advertisement-recommendation-case-study_2.13-0.1.jar "arg 1" arg2`
and  
`$ java -cp advertisement-recommendation-case-study_2.13-0.1.jar Naive_Approach "arg 1" arg2`
will not work and failed with:  
`Error: A JNI error has occurred, please check your installation and try again
Exception in thread "main" java.lang.NoClassDefFoundError: scala/math/Ordering`  
so instead using [Installing SDKMAN!](https://sdkman.io/install):
```sh
$ sdk i scala
```
and in a new terminal window:  
```sh
$ sdk use scala 2.13.0
Using scala version 2.13.0 in this shell.
$ scala -version
Scala code runner version 2.13.0 -- Copyright 2002-2019, LAMP/EPFL and Lightbend, Inc.
```  
so [JARs will run](https://alvinalexander.com/scala/sbt-how-build-single-executable-jar-file-assembly) as expected:  
```sh
$ scala advertisement-recommendation-case-study_2.13-0.1.jar "arg 1" arg2  
stdout > e.g. main.args(2):[`arg 1`, `arg2`]
```
to create a .zip distribution with [JavaAppPackaging](https://www.scala-sbt.org/1.x/docs/sbt-by-example.html#Reload+and+create+a+.zip+distribution):  
```sh
sbt:advertisement-recommendation-case-study> dist
[info] Packaging ../advertisement_recommendation_case_study/target/scala-2.13/advertisement-recommendation-case-study_2.13-0.1-sources.jar ...
[info] Wrote ../advertisement_recommendation_case_study/target/scala-2.13/advertisement-recommendation-case-study_2.13-0.1.pom
[info] Your package is ready in ../advertisement_recommendation_case_study/target/universal/advertisement-recommendation-case-study-0.1.zip
```
to run the packaged app:  
```sh
$ cd /path_for_unpacked_app
$ unzip -o -d /path_for_unpacked_app ../advertisement_recommendation_case_study/target/universal/advertisement-recommendation-case-study-0.1.zip
$ chmod +x advertisement-recommendation-case-study
$ ./advertisement-recommendation-case-study-0.1/bin/advertisement-recommendation-case-study <args string with space separated values>
<some std out if any>
```
### API:
--toDo  
format for path arguments  
to `impression` and `click` events files  
to pass to the application main method:  
`<path>impressions<ordinal>.json <path>clicks<ordinal>.json`  
> Note that:  
at least one path for each type of events must be provided  
or application will resort to  
hardcoded path used in development  
such as:  
`/src/test/resources/clicks.json` and `/src/test/resources/impressions.json`

### general workflow steps:
1. Develop locally.

## shutting down server:
inside VM running at `sbt`:  
`Ctrl+C`

## Running tests
( some unit tests ( is | have been ) implemented )
```sh
$ sbt
sbt:advertisement-recommendation-case-study> test
```

## Author
**Alex Glukhovtsev**

+ [github/GlulkAlex](https://github.com/GlulkAlex)
+ [twitter/@GlukAlex](https://twitter.com/GlukAlex)

## License
Copyright © 2019 Alex Glukhovtsev  
Released under the MIT license.

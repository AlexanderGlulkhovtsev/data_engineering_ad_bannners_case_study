ThisBuild / scalaVersion := "2.13.0"//>"2.12.8"
//ThisBuild / organization := "com.example"
name := "advertisement-recommendation-case-study"
version := "0.2"
// there were N deprecation warnings (since 2.13.0); 
// re-run with -deprecation for details
scalacOptions += "-deprecation"

// to change the logging level for compilation 
// to only show warnings and errors:
//> set 
logLevel in compile := Level.Warn
// To enable debug logging for all tasks in the current project,
//> set logLevel := Level.Warn
// to show stack traces up to the first sbt frame:
//> set every traceLevel := 0
//> set traceLevel in Test := 5
//> set traceLevel in update := 0
//> set traceLevel in ThisProject := -1

enablePlugins( JavaAppPackaging )

libraryDependencies += "org.scalactic" %% "scalactic" % "3.0.8"
libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.8" % "test"

// uJson comes bundled with uPickle, 
// or can be used stand-alone via the following package coordinates:
libraryDependencies += "com.lihaoyi" %% "ujson" % "0.7.5"

// val playJson  = "com.typesafe.play" %% "play-json" % "2.6.9"
// libraryDependencies += playJson

// for the JVM
//?libraryDependencies += "io.monix" %% "monix" % "3.0.0-RC2"


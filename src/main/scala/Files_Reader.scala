package data_mining

import java.util.concurrent.TimeUnit._

import java.io.RandomAccessFile
import java.io.IOException

import java.nio.ByteBuffer
import java.nio.channels.FileChannel

import scala.util.Using
import scala.util.{ Try, Success, Failure }

import scala.collection.JavaConverters._
import scala.collection.immutable.ArraySeq
//scala.collection.immutable.LazyList
import scala.collection.concurrent.TrieMap

import scala.concurrent.{ ExecutionContext, Future, Promise }
import scala.concurrent.duration._
//import ExecutionContext.Implicits.global

import Naive_Approach.{ Ad_Event, Ad_Click, Ad_Impression }


/**
/// @toDo: Implement
idea:
flatten a banch of file read results 
in one parallel collection 
as one Future value 
*/
object Files_Reader
{
  implicit val ec = ExecutionContext.global
  
  /**
  // java.io.IOException: Stream Closed
  */
  @throws[ IOException ]("if ? the file Stream Closed ?")
  def read_Chars_From_File(
    file_Path_Name: String// = "./src/test/resources/clicks.json" | "impressions.json"
  ): Array[ Char ] = //ArraySeq[ Char ] = 
    Using.resource(
      scala.io.Source.fromFile( file_Path_Name, "UTF8" )
    ){ reader =>
        // it has to be materialized inside befor resource release took place | happend
        //!LazyList
//         ArraySeq
//           .from(
            reader
              .toArray
              //!?.getBytes()
              //?.iterator 
//           )
          //! No implicit view available from Char => scala.collection.IterableOnce[B]
          //?.flatten
    }
  
  /**
  /// @toDo: what about "][" between concatenated files ?
  TrieMap.valuesIterator: Iterator[V]
  Chars per se is not of intrest in general
  parsed values are 
  e.g. in form of case class records as items | rows
  
  returns:
    collection of: file name -> file content of chars ?
  */
  def get_Files_Content(
    files_Paths_Names: Seq[ String ],
    //? events_Parser( input: Iterator[ Char ], record: Ad_Event )
    //?content_Parser: Iterable[ Char ] => Iterable[ Ad_Event ]
    content_Parser: Iterator[ Char ] => Iterator[ Ad_Event ]
  ): Iterable[ Ad_Event ] = //Char ] = //ArrayBuffer[ Char ]
    TrieMap
      .from( 
        files_Paths_Names
          /// @toDo: maybe parser needs to be injected here ?
          .map( name => ( name, read_Chars_From_File( name) ) )
      )
      .flatMap( kv => 
        // found   : Array[Char]
        // required: Iterator[Char]
        content_Parser( kv._2.iterator ) 
      )
  
  /** 
  /// @toDo: implement `get_Files_Content_Async`
  
  found   : [B](fn: String => scala.concurrent.Future[B])(implicit bf: scala.collection.BuildFrom[Seq[String],B,Seq[B]], implicit executor: scala.concurrent.ExecutionContext)scala.concurrent.Future[Seq[B]]
[error]  required: scala.concurrent.Future[Iterable[data_mining.Naive_Approach.Ad_Event]]

wrong number of type parameters for method traverse: [A, B, M[X] <: IterableOnce[X]](in: M[A])(fn: A => scala.concurrent.Future[B])(implicit bf: scala.collection.BuildFrom[M[A],B,M[B]], implicit executor: scala.concurrent.ExecutionContext)scala.concurrent.Future[M[B]]

  */
  def get_Files_Content_Async(
    files_Paths_Names: Seq[ String ],
    content_Parser: Iterator[ Char ] => Iterator[ Ad_Event ]
  ): Future[ 
    //Iterable[ 
      //Ad_Event 
      //Iterator[ Char ]
    //] 
    Seq[ 
      //Char 
      Ad_Event 
    ]
  ] = 
    Future
      //?.sequence(
      .traverse/*[ String, Ad_Event, Iterable[ Ad_Event ] <: Seq[ Ad_Event ] ]*/(
        files_Paths_Names
      )( name => 
        Future( 
          content_Parser( 
            read_Chars_From_File( name )
              .iterator // : Iterator[ Char ]
          ) 
        ) 
      )
      //?.map( c => content_Parser( c.iterator ) )
      //?.flatten
      .map( _.flatten )// : Seq[Char]
  
}

package data_mining

import scala.io.BufferedSource

import Naive_Approach.{ 
  Ad_Event, Ad_Click, Ad_Impression, 
}


/**
it has to try to parse ( valid ) JSON 
( with known upfront structure )
or fail miserably in the process
*/
object JSON_Parser {
  // helpers to compose:
  /** 
  // stop | terminate on ']' or input.isEmpty
  return -> e.g. Seq( "{...}", ..., "{...}" )
  */
  @scala.annotation.tailrec
  def get_Array_Items_Iter(//_0( 
    input: Iterator[ Char ], // | String
    //?last_Char: Char = '\n',// ? or '\t' | '\b' | '\r' ?
    // state cases flags:
    is_Array_Start: Boolean = false,
    is_Array_Stop: Boolean = false,
    is_Array_Item_Start: Boolean = false,
    //?is_Array_Item_Stop: Boolean = false,
    array_Item: Iterator[ Char ] = Iterator[ Char ](),
    // result
    array_Items: Iterator[ Iterator[ Char ] ] = Iterator[ Iterator[ Char ] ](),
    is_DeBug_Mode: Boolean = 1 == 0
  ): ( 
    // items 
    Iterator[ Iterator[ Char ] ], //Iterator[ String ]
    // rest 
    Iterator[ Char ]
  ) = if( 
    input.isEmpty || is_Array_Stop//last_Char == ']' 
  ){
    ( array_Items, input )
  }else{
    val current_Char: Char = input.next()
    // cases
    val (
      is_Array_Start_Updated: Boolean,
      is_Array_Stop_Updated: Boolean,
      is_Array_Item_Start_Updated: Boolean,
      array_Item_Updated: Iterator[ Char ],
      array_Items_Updated: Iterator[ Iterator[ Char ] ]
    ) = current_Char match {
      case '[' => 
        if( is_DeBug_Mode ){ Console.err.println( s"""${current_Char} -> Array_Start case""" ) }
      ( 
        true, is_Array_Stop, is_Array_Item_Start, array_Item, array_Items )
      case '{' => 
        if( is_DeBug_Mode ){ Console.err.println( s"""${current_Char} -> Array_Item_Start case""" ) }
      ( 
        is_Array_Start, is_Array_Stop, true, Iterator[ Char ](), array_Items )
      case '}' => 
        if( is_DeBug_Mode ){ Console.err.println( s"""${current_Char} -> Array_Item_Stop case""" ) }
      ( 
        is_Array_Start, is_Array_Stop, false, 
        Iterator[ Char ](), array_Items.++( Iterator( array_Item ) ) )
      case ']' => 
        if( is_DeBug_Mode ){ Console.err.println( s"""${current_Char} -> Array_Stop case""" ) }
      ( 
        is_Array_Start, true, is_Array_Item_Start, array_Item, array_Items )
      case _ => if( is_Array_Start && is_Array_Item_Start ){
          if( is_DeBug_Mode ){ Console.err.println( s"""${current_Char} -> add to Array_Item case""" ) }
          ( 
            is_Array_Start, is_Array_Stop, is_Array_Item_Start, 
            array_Item.++( Iterator( current_Char ) ), array_Items )
        }else{
          if( is_DeBug_Mode ){ Console.err.println( s"""skip ${current_Char} case""" ) }
          ( 
            is_Array_Start, is_Array_Stop, is_Array_Item_Start, 
            array_Item, array_Items )
        }
    }
    
    get_Array_Items_Iter( 
      input = input,
      //last_Char = current_Char//input.next()
      is_Array_Start = is_Array_Start_Updated,
      is_Array_Stop = is_Array_Stop_Updated,
      is_Array_Item_Start = is_Array_Item_Start_Updated,
      array_Item = array_Item_Updated,
      array_Items = array_Items_Updated,
      is_DeBug_Mode = is_DeBug_Mode
    )
  }

  @scala.annotation.tailrec
  def get_Array_Items_Iter_1( 
    input: Iterator[ Char ], 
    //?array_Item: Iterator[ Char ] = Iterator[ Char ](),
    // result
    array_Items: Iterator[ Iterator[ Char ] ] = Iterator[ Iterator[ Char ] ](),
    is_DeBug_Mode: Boolean = 1 == 0
  ): ( Iterator[ Iterator[ Char ] ], Iterator[ Char ] ) = 
    if( input.isEmpty ){
      if( is_DeBug_Mode ){ Console.err.println( s"""basic case ${array_Items.knownSize}""" ) }
      ( array_Items, input )
    }else{
      if( is_DeBug_Mode ){ Console.err.println( s"""recursive case ${input.knownSize}""" ) }
      val array_Item: Iterator[ Char ] = input
        .dropWhile( _ != '{' )
        .drop(1)
        .takeWhile( _ != '}' ) 
      // java.lang.AssertionError: assertion failed: expected nonEmpty array_Item
      assert( array_Item.nonEmpty, "expected nonEmpty array_Item" )
      
      get_Array_Items_Iter_1( 
        // mutated
        input = input,
        //! java.util.concurrent.ExecutionException: java.lang.OutOfMemoryError: GC overhead limit exceeded
        array_Items = array_Items.++( 
          Iterator(
            //input.dropWhile( _ != '{' ).drop(1).takeWhile( _ != '}' ) 
            array_Item
          )
        ),
        is_DeBug_Mode = is_DeBug_Mode
      )
    }
  
  def key_Value_Extractor( 
    kV_Iter: Iterator[ Char ] 
  ): ( Iterator[ Char ], Iterator[ Char ] ) = {
    // prefix
    //val key = kV_Iter.takeWhile( _ != ':')
    // rest as suffix
    ( kV_Iter.takeWhile( _ != ':'), kV_Iter )
  }
  /**
  // stop | terminate on ( first unpaired ) '}' or input.isEmpty
  
  return -> Seq( "\"$key\":$val_Str", ..., "\"$key\":$val_Str" )
  */
  @scala.annotation.tailrec
  def get_Object_KV_Pairs_Iter_0( 
    input: Iterator[ Char ], // | String 
    // state cases flags:
    is_Key_Start: Boolean = false,
    //?is_Key_Stop: Boolean = false,
    is_Value_Start: Boolean = false,
    kV_Pair: ( String, String ) = ( "", "" ),
    // result
    kV_Pairs: Iterator[ ( String, String ) ] = Iterator[ ( String, String ) ](),
    is_DeBug_Mode: Boolean = 1 == 0
  ): ( 
    // items 
    Iterator[ 
      ( String, String )// | 
      //( String, Iterator[ Char ] ) |
      //( Iterator[ Char ], Iterator[ Char ] )
    ], 
    // rest 
    Iterator[ Char ]
  ) = if( 
    input.isEmpty 
  ){
    //( Iterator( ( "key", "value" ) ), input )
    //val ( k, v ) = kV_Pair
    kV_Pair match {
      case ( "", "" ) => ( kV_Pairs, input )
      case ( k, v ) => 
          if( is_DeBug_Mode ){ Console.err.println( s"""adding last ${kV_Pair}""" ) }
        ( 
          kV_Pairs.++( 
            Iterator( 
              //kV_Pair 
              ( k, v.trim() )
            ) 
          ), 
          input 
        )
    }
  }else{
    val current_Char: Char = input.next()
    // cases
    val (
      is_Key_Start_Updated: Boolean,
      //?is_Key_Stop_Updated: Boolean,
      is_Value_Start_Updated: Boolean,
      kV_Pair_Updated: ( String, String ),
      kV_Pairs_Updated: Iterator[ ( String, String ) ]
    ) = current_Char match {
      /// @Observation: it somehow skips enclosing '"' for value ?
      case '"' if !is_Value_Start => if( is_Key_Start ){
          if( is_DeBug_Mode ){ Console.err.println( s"""${current_Char} -> Key_Stop case""" ) }
          ( false, is_Value_Start, kV_Pair, kV_Pairs )
        }else{ // !is_Key_Start
          if( is_DeBug_Mode ){ Console.err.println( s"""${current_Char} -> Key_Start case""" ) }
          ( true, is_Value_Start, ( "", "" ), kV_Pairs )
        }
      case ':' => 
        if( is_DeBug_Mode ){ Console.err.println( s"""${current_Char} -> Value_Start case""" ) }
        ( is_Key_Start, true, kV_Pair, kV_Pairs )
      case ',' => {
          val ( k, v ) = kV_Pair
          if( is_DeBug_Mode ){ 
            Console.err.println( 
              s"""${current_Char} -> Value_Stop_End case -> add ( $k, ${v.trim()} )""" ) }
          ( 
            is_Key_Start, false, ( "", "" ), 
            kV_Pairs.++( 
              Iterator( 
                //kV_Pair 
                ( k, v.trim() )
              ) 
            ) 
          )
        }
      case _ => if( is_Key_Start ){
          // !is_Value_Start => add to Key
          if( is_DeBug_Mode ){ Console.err.println( s"""${current_Char} -> add to Key case""" ) }
          val ( k, v ) = kV_Pair
          ( is_Key_Start, is_Value_Start, ( k + current_Char, v ), kV_Pairs )
        }else if( is_Value_Start ){
          if( is_DeBug_Mode ){ Console.err.println( s"""${current_Char} -> add to Value case""" ) }
          val ( k, v ) = kV_Pair
          ( is_Key_Start, is_Value_Start, ( k, v + current_Char ), kV_Pairs )
        }else{// !is_Key_Start && !is_Value_Start -> skip
          if( is_DeBug_Mode ){ Console.err.println( s"""skip ${current_Char} case""" ) }
          ( is_Key_Start, is_Value_Start, kV_Pair, kV_Pairs )
        }
    }
    
    get_Object_KV_Pairs_Iter_0( 
      input = input,
      is_Key_Start = is_Key_Start_Updated,
      //?is_Key_Stop = is_Key_Stop_Updated,
      is_Value_Start = is_Value_Start_Updated,
      kV_Pair = kV_Pair_Updated,
      kV_Pairs = kV_Pairs_Updated,
      is_DeBug_Mode = is_DeBug_Mode
    )
  }
  
  @scala.annotation.tailrec
  def get_Object_KV_Pairs_Iter( //_1(
    input: Iterator[ Char ], 
    // result
    kV_Pairs: Iterator[ ( String, String ) ] = Iterator[ ( String, String ) ](),
    is_DeBug_Mode: Boolean = 1 == 0
  ): ( Iterator[ ( String, String ) ], Iterator[ Char ] ) = 
    if( input.isEmpty ){
      if( is_DeBug_Mode ){ Console.err.println( s"""basic case ${kV_Pairs.knownSize}""" ) }
      ( kV_Pairs, input )
    }else{
      if( is_DeBug_Mode ){ Console.err.println( s"""recursive case ${input.knownSize}""" ) }
      // str.headOption.fold("")( _ => str.drop(1).take( str.size - 2 ) )
      //val key: String = 
      val s"""$prefix"${key}"$suffix""" = 
        input
          .dropWhile( _ != '"' )
          .takeWhile( _ != ':' )
          .mkString
      assert( key.nonEmpty, "expected nonEmpty key" )
      /// @toDo: what about JSON string value ? quoted | enclosed in double quotation marks ?
      val value: String = 
        input
          .takeWhile( _ != ',' )
          .mkString
          .trim()

      get_Object_KV_Pairs_Iter(//_1( 
        // mutated
        input = input,
        kV_Pairs = kV_Pairs.++( 
          Iterator(
            ( key, value )
          )
        ),
        is_DeBug_Mode = is_DeBug_Mode
      )
    }
  
  trait JSON_Scalar {
    val value: Any
    //?def get: Any = value
  }
  case class JSON_String( value: String ) extends JSON_Scalar
  case class JSON_Integer( value: Int ) extends JSON_Scalar
  case class JSON_Float( value: Double ) extends JSON_Scalar
  object JSON_Null extends JSON_Scalar {
    val value = None
  }
  /**
  // ? recursive ? in general 
  // non general, but specific and limited
  return -> String | Int | float as Double | null as None 
  */  
  @throws[ MatchError ]("""e.g. in ? case "true" | "false" ?""")
  def get_Value(//_Iter( 
    input: String// | Iterator[ Char ] 
  ): JSON_Scalar = 
    input.trim() match {
      case "null" | "" => JSON_Null
      // str @ 
      case s""""${string_Value}"""" => JSON_String( string_Value )
      //case "true" | "false" => not expected
      case number => number match {
        // float @ 
        case s"$int.$decimal" => JSON_Float( java.lang.Double.parseDouble( number ) )
        case _ => JSON_Integer( Integer.parseInt( number ) )
      }
    }
  
  /** 
  /// @toDo: how to assign field(s) values by its known name ?
  /// and cast it to the right type ?
scala> Cct.getClass.getDeclaredMethods()(3).getParameterTypes().map( _.getTypeName()).zip(Seq(1,"a",42.0,Some("EU")))
res104: Array[(String, Any)] = Array((int,1), (java.lang.String,a), (double,42.0), (scala.Option,Some(EU)))
scala> Seq(1,"a",42.0,Some("EU")).zip(Cct.getClass.getDeclaredMethods()(3).getParameterTypes().map( _.getTypeName()))
res105: Seq[(Any, String)] = List((1,int), (a,java.lang.String), (42.0,double), (Some(EU),scala.Option))

  */
  def events_Parser( 
    input: Iterator[ Char ], 
    //>>>
    record_Builder: Iterator[ String ] => Ad_Event,
    // class or instance ?
    //?record: Ad_Event//?.type
    is_DeBug_Mode: Boolean = 1 == 0
  ): Iterator[ Ad_Event ] = {
//     val apply_To = 
//       record
//         .getClass
//         .getDeclaredMethods()//(3)
//         // or
//         .filter( _.getName() == "apply" )
//         //! java.util.NoSuchElementException: head of empty array
//         .head
    
    // works only for case classes with defaults
    //?val fields_Set = record().productElementNames.toSet
    
//     val fields_Type_Names: Array[ String ] = 
//       record
//         .getClass
//         .getDeclaredMethods()(3)
//         .getParameterTypes()
//         .map( _.getTypeName() )
    
    val ( 
      // items 
      arr_Items_Iter: Iterator[ Iterator[ Char ] ], //Iterator[ String ]
      // rest 
      _// : Iterator[ Char ]
    ) = get_Array_Items_Iter( input = input, is_DeBug_Mode = is_DeBug_Mode )
    // found   : Iterator[Object]
    // required: Iterable[data_mining.Naive_Approach.Ad_Event]
    arr_Items_Iter
      .map( arr_Item => {
            // Seq[Any |? JSON_Scalar ?]( ? 1,"a",true ? )
            // assuming same Ad_Event field names as keys
          val ( 
            kv_Pairs_Iter: Iterator[ ( String, String ) ], 
            _ // rest : Iterator[ Char ]
          ) = get_Object_KV_Pairs_Iter( 
            input = arr_Item, is_DeBug_Mode = is_DeBug_Mode )
          
          val values: Iterator[ String ] = 
            kv_Pairs_Iter
              .map( _._2 )
//               .zip( fields_Type_Names )
//               .map{ case ( v, v_Type: String ) => ??? }

          record_Builder( values )
//           apply_To
//             //! java.lang.IllegalArgumentException: wrong number of arguments
//             .invoke( 
//               record, 
//               // required: Seq[Object]
//               values.toSeq: _* 
//             )
//             .asInstanceOf[ Ad_Event ]
        }
      )
  }
  
  /** specialized */
  def ad_Clicks_Parser( 
    input: Iterator[ Char ] 
  ): Iterator[ Ad_Event ] =
    events_Parser( 
      input = input, 
      record_Builder = Ad_Click.from_Seq
//       ( in: Iterator[ _ ] ) => 
//         //!?! scala.MatchError: List("97dd2a0f-6d42-4c63-8cd6-5270c19f20d6", 2.091225600111518)
//         in.take(2).toSeq match { 
//           // for alternative get_Object_KV_Pairs_Iter ?
//           case List( s""""$i"""", r: String ) => Ad_Click( i, java.lang.Double.parseDouble( r ) ) 
//           //?case Seq( i: String, r: Double ) => Ad_Click( i, r ) 
//           //!case List( i: String, r: String ) => Ad_Click( i, get_Value( r ).value )
//           case List( i: String, r: String ) => Ad_Click( i, java.lang.Double.parseDouble( r ) ) 
//         }
      //?record = Ad_Click() 
    )
  
  def ad_Impressions_Parser( 
    input: Iterator[ Char ] 
  ): Iterator[ Ad_Event ] =
    events_Parser( 
      input = input, 
      record_Builder = Ad_Impression.from_Seq
//       ( in: Iterator[ _ ] ) => 
//         //! scala.MatchError: List(32, 8, "UK", "a39747e8-9c58-41db-8f9f-27963bc248b5")
//         in.take(4).toSeq match { 
//           //?case Seq( p: Int, d: Int, c_Str: String, i: String ) => {
//           case Seq( p: String, d: String, c_Str: String, s""""${i}"""" ) => {
//             val c: Option[ String ] = c_Str match {
//               case "null" | "" | "\"\"" => None
//               //?
//               case s""""${string_Value}"""" => Some( string_Value )
//               case _ => Some( c_Str )
//             }
//             Ad_Impression( Integer.parseInt(p), Integer.parseInt(d), c, i ) 
//           }
//         }
      //?record = Ad_Impression() 
    )

}

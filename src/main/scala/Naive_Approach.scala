package data_mining

import java.util.concurrent.{ Executors, TimeUnit }
//import java.util.concurrent.TimeUnit._
//import java.util.concurrent.TimeUnit.MILLISECONDS

import java.io.RandomAccessFile
import java.io.IOException

import java.nio.ByteBuffer
import java.nio.channels.FileChannel

import scala.util.Using
import scala.util.{ Try, Success, Failure }

import scala.collection.JavaConverters._
import scala.collection.immutable.ArraySeq
import scala.collection.concurrent.TrieMap

import scala.concurrent.{ ExecutionContext, Future, Promise }
import scala.concurrent.duration._


/**
[task](https://?!?/)

*/
object Naive_Approach//Solution 
extends App
{
  //implicit val ec = ExecutionContext.global
  val executorService: java.util.concurrent.ExecutorService = Executors
    //>.newWorkStealingPool( 8 )
    .newCachedThreadPool
  implicit val ec: scala.concurrent.ExecutionContextExecutorService = 
    ExecutionContext.fromExecutorService( executorService )
  
  sealed 
  trait Ad_Event 
  /** static method: alternative constructor */
  object Ad_Click { 
    @throws[ MatchError ]("""e.g. in not exact cases""")
    //?implicit 
    def from_Seq( in: Iterable[ _ ] ): Ad_Event = 
      in.take(2) match { 
        case Seq( i: String, r: Double ) => Ad_Click( i, r ) 
      } 
    /** overloading */
    def from_Seq( in: Iterator[ String ] ): Ad_Event =
        in.take(2).toSeq match { 
          // for alternative get_Object_KV_Pairs_Iter ?
          case List( s""""$i"""", r: String ) => 
            Ad_Click( i, java.lang.Double.parseDouble( r ) ) 
          case List( i: String, r: String ) => 
            Ad_Click( i, java.lang.Double.parseDouble( r ) ) 
        }
  }
  case class Ad_Click( 
    impression_id: String = "97dd2a0f-6d42-4c63-8cd6-5270c19f20d6",
    revenue: Double = 2.091225600111518
  ) extends Ad_Event 
  val ad_Click_Ordering = 
    Ordering
      .by[ Ad_Click, String ]( _.impression_id )
      //>.orElseBy[ Double ]( _.revenue )( Ordering.Double.TotalOrdering )
      .orElse( Ordering.Double.TotalOrdering.on( _.revenue ) )
  /// expected total: 2014 objects
  /** static method: alternative constructor */
  object Ad_Impression { 
    @throws[ MatchError ]("""e.g. in not exact cases""")
    //?implicit 
    def from_Seq( in: Iterable[ _ ] ): Ad_Event = 
      in.take(4) match { 
        case Seq( p: Int, d: Int, c: Option[ String ], i: String ) => 
          Ad_Impression( p, d, c, i ) 
      } 
    /** overloading */
    def from_Seq( in: Iterator[ String ] ): Ad_Event =
        in.take(4).toSeq match { 
          case Seq( 
            p: String, d: String, c_Str: String, s""""${i}"""" 
          ) => {
            val c: Option[ String ] = c_Str match {
              case "null" | "" | "\"\"" => None
              //?
              case s""""${string_Value}"""" => Some( string_Value )
            }
            Ad_Impression( Integer.parseInt(p), Integer.parseInt(d), c, i ) 
          }
        }
  }
  case class Ad_Impression( 
    app_id: Int = 32,
    advertiser_id: Int = 8,
    country_code: Option[ String ] = None, //"UK",// or null or ""
    id: String = "a39747e8-9c58-41db-8f9f-27963bc248b5"
  ) extends Ad_Event 
  val ad_Impression_Ordering = 
    Ordering
      .by[ Ad_Impression, Int ]( _.app_id )
      .orElseBy[ Int ]( _.advertiser_id )
      .orElseBy[ Option[ String ] ]( _.country_code )
      .orElseBy[ String ]( _.id )
  
  object Ad_Event_Ordering extends Ordering[ Ad_Event ] {
    //def compare(x:U, y:U) = Ordering[T].compare(f(x), f(y))
    def compare(
      a1: Ad_Event, 
      a2: Ad_Event
    ) = 
      //a.age compare b.age
      ( a1, a2 ) match {
        case ( c1: Ad_Click, c2: Ad_Click ) => ad_Click_Ordering
          .compare( c1, c2 )
        case ( i1: Ad_Impression, i2: Ad_Impression ) => ad_Impression_Ordering
          .compare( i1, i2 )
        case ( c: Ad_Click, i: Ad_Impression ) => Ordering[ String ]
          .compare( c.impression_id, i.id )
        case ( i: Ad_Impression, c: Ad_Click ) => Ordering[ String ]
          .compare( i.id, c.impression_id )
      }
  }

  @scala.annotation.tailrec
  def get_Array_Items_Iter(
    input: Iterator[ Char ], // | String
    // state cases flags:
    is_Array_Start: Boolean = false,
    is_Array_Stop: Boolean = false,
    is_Array_Item_Start: Boolean = false,
    array_Item: Iterator[ Char ] = Iterator[ Char ](),
    // result
    array_Items: Iterator[ Iterator[ Char ] ] = Iterator[ Iterator[ Char ] ](),
    is_DeBug_Mode: Boolean = 1 == 0
  ): ( 
    // items 
    Iterator[ Iterator[ Char ] ], //Iterator[ String ]
    // rest 
    Iterator[ Char ]
  ) = if( 
    input.isEmpty || is_Array_Stop
  ){
    ( array_Items, input )
  }else{
    val current_Char: Char = input.next()
    // cases
    val (
      is_Array_Start_Updated: Boolean,
      is_Array_Stop_Updated: Boolean,
      is_Array_Item_Start_Updated: Boolean,
      array_Item_Updated: Iterator[ Char ],
      array_Items_Updated: Iterator[ Iterator[ Char ] ]
    ) = current_Char match {
      case '[' => 
        if( is_DeBug_Mode ){ Console.err.println( s"""${current_Char} -> Array_Start case""" ) }
      ( 
        true, is_Array_Stop, is_Array_Item_Start, array_Item, array_Items )
      case '{' => 
        if( is_DeBug_Mode ){ Console.err.println( s"""${current_Char} -> Array_Item_Start case""" ) }
      ( 
        is_Array_Start, is_Array_Stop, true, Iterator[ Char ](), array_Items )
      case '}' => 
        if( is_DeBug_Mode ){ Console.err.println( s"""${current_Char} -> Array_Item_Stop case""" ) }
      ( 
        is_Array_Start, is_Array_Stop, false, 
        Iterator[ Char ](), array_Items.++( Iterator( array_Item ) ) )
      case ']' => 
        if( is_DeBug_Mode ){ Console.err.println( s"""${current_Char} -> Array_Stop case""" ) }
      ( 
        is_Array_Start, true, is_Array_Item_Start, array_Item, array_Items )
      case _ => if( is_Array_Start && is_Array_Item_Start ){
          if( is_DeBug_Mode ){ Console.err.println( s"""${current_Char} -> add to Array_Item case""" ) }
          ( 
            is_Array_Start, is_Array_Stop, is_Array_Item_Start, 
            array_Item.++( Iterator( current_Char ) ), array_Items )
        }else{
          if( is_DeBug_Mode ){ Console.err.println( s"""skip ${current_Char} case""" ) }
          ( 
            is_Array_Start, is_Array_Stop, is_Array_Item_Start, 
            array_Item, array_Items )
        }
    }
    
    get_Array_Items_Iter( 
      input = input,
      is_Array_Start = is_Array_Start_Updated,
      is_Array_Stop = is_Array_Stop_Updated,
      is_Array_Item_Start = is_Array_Item_Start_Updated,
      array_Item = array_Item_Updated,
      array_Items = array_Items_Updated,
      is_DeBug_Mode = is_DeBug_Mode
    )
  }

  @scala.annotation.tailrec
  def get_Object_KV_Pairs_Iter( 
    input: Iterator[ Char ], 
    // result
    kV_Pairs: Iterator[ ( String, String ) ] = Iterator[ ( String, String ) ](),
    is_DeBug_Mode: Boolean = 1 == 0
  ): ( Iterator[ ( String, String ) ], Iterator[ Char ] ) = 
    if( input.isEmpty ){
      if( is_DeBug_Mode ){ Console.err.println( s"""basic case ${kV_Pairs.knownSize}""" ) }
      ( kV_Pairs, input )
    }else{
      if( is_DeBug_Mode ){ Console.err.println( s"""recursive case ${input.knownSize}""" ) }
      // str.headOption.fold("")( _ => str.drop(1).take( str.size - 2 ) )
      //val key: String = 
      val s"""$prefix"${key}"$suffix""" = 
        input
          .dropWhile( _ != '"' )
          .takeWhile( _ != ':' )
          .mkString
      assert( key.nonEmpty, "expected nonEmpty key" )
      val value: String = 
        input
          .takeWhile( _ != ',' )
          .mkString
          .trim()

      get_Object_KV_Pairs_Iter(
        input = input,
        kV_Pairs = kV_Pairs.++( 
          Iterator(
            ( key, value )
          )
        ),
        is_DeBug_Mode = is_DeBug_Mode
      )
    }

  def events_Parser( 
    input: Iterator[ Char ], 
    record_Builder: Iterator[ String ] => Ad_Event,
    is_DeBug_Mode: Boolean = 1 == 0
  ): Iterator[ Ad_Event ] = {
    val ( 
      // items 
      arr_Items_Iter: Iterator[ Iterator[ Char ] ], //Iterator[ String ]
      // rest 
      _// : Iterator[ Char ]
    ) = get_Array_Items_Iter( input = input, is_DeBug_Mode = is_DeBug_Mode )
    arr_Items_Iter
      .map( arr_Item => {
          val ( 
            kv_Pairs_Iter: Iterator[ ( String, String ) ], 
            _ // rest : Iterator[ Char ]
          ) = get_Object_KV_Pairs_Iter( 
            input = arr_Item, is_DeBug_Mode = is_DeBug_Mode )
          
          val values: Iterator[ String ] = 
            kv_Pairs_Iter
              .map( _._2 )

          record_Builder( values )
        }
      )
  }
  
  /** specialized */
  def ad_Clicks_Parser( 
    input: Iterator[ Char ] 
  ): Iterator[ Ad_Event ] =
    events_Parser( 
      input = input, 
      record_Builder = Ad_Click.from_Seq
    )
  
  def ad_Impressions_Parser( 
    input: Iterator[ Char ] 
  ): Iterator[ Ad_Event ] =
    events_Parser( 
      input = input, 
      record_Builder = Ad_Impression.from_Seq
    )

  @throws[ IOException ]("if ? the file doesn't exist ?")
  def write_Str_2_File_with_FileChannel( 
    file_Path_Name: String,
    str_Data: String// = "Hello"
  ): Unit = 
    Using.Manager { use => {
      // resource
      val f_Stream: RandomAccessFile = use(
        new RandomAccessFile( file_Path_Name , "rw" ) )
      val f_Channel: FileChannel = use( f_Stream.getChannel() )
      
      val str_Bytes: Array[ Byte ] = str_Data.getBytes()
      val buffer: ByteBuffer = ByteBuffer.allocate( str_Bytes.length )
      
      buffer.put( str_Bytes )
      buffer.flip()
      f_Channel.write( buffer )
    } }
  
  if( 1 == 0 ){// <- unit test
  write_Str_2_File_with_FileChannel(
    "metrics.json",
    """[{"app_id": 1, "country_code": "US", "impressions": 102, "clicks": 12, "revenue"}]"""
  )
  }
  
  @throws[ IOException ]("if the file's path doesn't exist ? or not enough write permissions ?")
  def write_Text_Lines_To_File_With_FileChannel( 
    file_Path_Name: String,
    text_Lines: Iterator[ String ]
  ): Unit = {
    println( s"About to handle `$file_Path_Name` file resource with Using.Manager ..." )
    Using.Manager { use => {
        // resource
        val f_Stream: RandomAccessFile = use(
          new RandomAccessFile( file_Path_Name , "rw" ) )
        val f_Channel: FileChannel = use( f_Stream.getChannel() )
        
        for( text_Line <- text_Lines ){
          val str_Bytes: Array[ Byte ] = text_Line.getBytes()
          val buffer: ByteBuffer = ByteBuffer.allocate( str_Bytes.length )
          
          buffer.put( str_Bytes )
          buffer.flip()
          f_Channel.write( buffer )
        }
      } 
    }
    println( s"Expecting Using.Manager to release `$file_Path_Name` file resource" )
  }
  // main.args(2):[`./src/test/resources/clicks_1.json`, `./src/test/resources/impressions_3.json`]
  println( 
    s"""main.args(${args.length}):""" + 
      args.map( s => "`" + s + "`").mkString("[", ", ", "]")
  ) 
  // expected arguments:
  // "{ clicks: [ ./src/test/resources/clicks1.json, ... ], impressions: [ ./src/test/resources/impressions1.json, ... ] }"
  // or simply: <path>clicks<1>.json ... <path>clicks<N>.json <path>impressions<1>.json ... 
  // e.g:
  // ./src/test/resources/clicks_1.json ./src/test/resources/impressions_3.json
  def extract_Paths_From_Args( 
    args: Array[ String ] = args,
    is_DeBug_Mode: Boolean = 1 == 0
  ): Map[ String, Seq[ String ] ] =
    args.foldLeft( Map[ String, Seq[ String ] ]() ){
      ( accum, arg ) => arg match { 
        case file @ s"${path}clicks${ord}.json" => 
          if( is_DeBug_Mode ){ Console.err.println( s"""clicks case for ${arg}""" ) }
          accum.updated( "clicks", accum.getOrElse( "clicks", Seq[ String ]() ).:+(  file ) )
        case file @ s"${path}impressions${ord}.json" => 
          if( is_DeBug_Mode ){ Console.err.println( s"""impressions case for ${arg}""" ) }
          accum.updated( "impressions", accum.getOrElse( "impressions", Seq[ String ]() ).:+(  file ) )
        case _ => 
          if( is_DeBug_Mode ){ Console.err.println( s"""other case for ${arg}""" ) }
          accum
    } }
  val paths_Map: Map[ String, Seq[ String ] ] = extract_Paths_From_Args()
  println( 
    s"""paths_Map(${paths_Map.size}):\n""" + 
        paths_Map
          .mkString("\n") 
  ) 

  @throws[ IOException ]("if ? the file Stream Closed ?")
  def read_Chars_From_File(
    file_Path_Name: String// = "./src/test/resources/clicks.json" | "impressions.json"
  ): Array[ Char ] = //ArraySeq[ Char ] = 
    Using.resource(
      scala.io.Source.fromFile( file_Path_Name, "UTF8" )
    ){ reader =>
        // it has to be materialized inside befor resource release took place | happend
        reader
          .toArray
    }
  
  /** 
  */
  def get_Files_Content(
    files_Paths_Names: Seq[ String ],
    content_Parser: Iterator[ Char ] => Iterator[ Ad_Event ]
  ): Iterable[ Ad_Event ] = //Char ] = //ArrayBuffer[ Char ]
    TrieMap
      .from( 
        files_Paths_Names
          .map( name => ( name, read_Chars_From_File( name) ) )
      )
      .flatMap( kv => 
        content_Parser( kv._2.iterator ) 
      )
  
  def get_Files_Content_Async(
    files_Paths_Names: Seq[ String ],
    content_Parser: Iterator[ Char ] => Iterator[ Ad_Event ]
  ): Future[ Seq[ Ad_Event ] ] = 
    Future
      .traverse( files_Paths_Names )( name => 
        Future( 
          content_Parser( 
            read_Chars_From_File( name )
              .iterator // : Iterator[ Char ]
          ) 
        ) 
      )
      .map( _.flatten )// : Seq[Char]
  
  val clicks_List: Future[ Seq[ 
    Ad_Event 
  ] ] = 
    get_Files_Content_Async( 
         files_Paths_Names = paths_Map
           .getOrElse( "clicks", Seq( "./src/test/resources/clicks.json" ) ),
         content_Parser = ad_Clicks_Parser
       )
    .andThen {
      case _ => println( ".andThen: `clicks_List` Future expected to be completed" )
    }
  clicks_List.foreach{ cL => 
    val cL_Sorted = cL
      .sorted( 
        Ad_Event_Ordering
      )
  println( Ad_Click().productElementNames.toList )
  println( 
    s"""clicks_List(${cL.size}):\n""" + 
      cL_Sorted
        .take( 11 )
        .mkString( "[\n", ",\n", ",\n..." ) + "\n" + cL_Sorted.last + "\n]"
  ) 
  }//?( executor = ExecutionContext.fromExecutor( Executors.newSingleThreadExecutor() ) )
  
  val revenue_Per_Impression_Map: Future[ Map[ 
    String, 
    Seq[ Double ]
  ] ] = clicks_List
    .map( _.groupMap( _.asInstanceOf[ Ad_Click ].impression_id )( _.asInstanceOf[ Ad_Click ].revenue ) )
    .andThen {
      case _ => println( 
        ".andThen: `revenue_Per_Impression_Map` Future expected to be completed" )
    }
  revenue_Per_Impression_Map.foreach{ rPIM => 
  println( 
    s"""revenue_Per_Impression_Map(${rPIM.size}):\n""" + 
        rPIM//revenue_Per_Impression_Map
          .toList
          .sortBy( _._2.size )(
            Ordering[ Int ]
              .reverse
          )
          .take(5)
          .mkString("\n") + 
    "..." ) 
  //}
  // of "impression_id"s: java.util.UUID
  val impressions: Set[ String ] = 
    rPIM//revenue_Per_Impression_Map
      .keySet
  // impressions(56):
  println( 
    s"""impressions(${impressions.size}):\n""" + 
        impressions
          .take(5)
          .mkString("\n") + 
    "..." ) 
  }//?( executor = ExecutionContext.fromExecutor( Executors.newSingleThreadExecutor() ) )
  
  val impressions_List: Future[ Seq[ Ad_Event ] ] = 
    get_Files_Content_Async(
        files_Paths_Names = paths_Map
          .getOrElse( 
            "impressions", Seq( "./src/test/resources/impressions.json" ) ),
        content_Parser = ad_Impressions_Parser
      )
    .andThen {
      case _ => println( 
        ".andThen: `impressions_List` Future expected to be completed" )
    }
  impressions_List.foreach{ iL => 
  println( Ad_Impression().productElementNames.mkString( ", " ) )
  /// @Done: checked for duplicates | repeated ids in impressions events
  println( 
    s"""impressions_List(${iL.size}):\n""" + 
        iL//impressions_List
          //! match may not be exhaustive.
          .sortBy{ case Ad_Impression( _, _, _, id ) => id } 
          .take(7)
          .mkString("\n") + 
    "..." ) 
  //}
  // of "app_id"s
  val applications: Set[ Int ] = 
    iL//impressions_List
      .foldLeft( Set[ Int ]() ){ case ( 
          accum, 
          Ad_Impression( app_id, advertiser_id, country_code, id )
        ) => 
        accum.union( Set( app_id ) ) }
  println( 
    s"""applications(${applications.size}):\n""" + 
        applications
          .take(5)
          .mkString("\n") + 
    "..." ) 
  // of "advertiser_id"s
  val advertisers: Set[ Int ] = 
    iL//impressions_List
      .foldLeft( Set[ Int ]() ){ case ( 
          accum, 
          Ad_Impression( app_id, advertiser_id, country_code, id )
        ) => 
        accum.union( Set( advertiser_id ) ) }
  println( 
    s"""advertisers(${advertisers.size}):\n""" + 
        advertisers
          .take(5)
          .mkString("\n") + 
    "..." ) 
  // of "country_code"s
  val countries_codes: Set[ Option[ String ] ] = 
    iL//impressions_List
      .foldLeft( Set[ Option[ String ] ]() ){ case ( 
          accum, 
          Ad_Impression( app_id, advertiser_id, country_code, id )
        ) => 
        accum.union( Set( country_code ) ) }
  println( 
    s"""countries_codes(${countries_codes.size}):\n""" + 
        countries_codes
          .take(5)
          .mkString("\n") + 
    "..." ) 
  
  val advertisers_Per_Application: Map[ Int, Set[ Int ] ] = 
    iL//impressions_List
      .foldLeft( Map[ Int, Set[ Int ] ]() ){ case ( 
          accum, 
          Ad_Impression( app_id, advertiser_id, country_code, id )
        ) => {
          val old_Set: Set[ Int ] = accum
            .getOrElse( app_id, Set[ Int ](  ) )
          
          accum.updated( 
            app_id, 
            old_Set.union( Set( app_id ) ) 
          ) 
        }
      }
  println( 
    s"""advertisers_Per_Application(${advertisers_Per_Application.size}):\n""" + 
        advertisers_Per_Application
          .take(5)
          .mkString("\n") + 
    "..." ) 
  
  val advertisers_Per_Country: Map[ Option[ String ], Set[ Int ] ] = 
    iL//impressions_List
      .foldLeft( Map[ Option[ String ], Set[ Int ] ]() ){ case ( 
          accum, 
          Ad_Impression( app_id, advertiser_id, country_code, id )
        ) => {
          val old_Set: Set[ Int ] = accum
            .getOrElse( country_code, Set[ Int ](  ) )
          
          accum.updated( 
            country_code, 
            old_Set.union( Set( advertiser_id ) ) 
          ) 
        }
      }
  println( 
    s"""advertisers_Per_Country(${advertisers_Per_Country.size}):\n""" +
        advertisers_Per_Country
          .take(5)
          .mkString("\n") + 
    "..." ) 
  }//?( executor = ExecutionContext.fromExecutor( Executors.newSingleThreadExecutor() ) )
  
  type App_Country_Key = ( Int, Option[ String ] )
  type Fields_Sums_Value = ( Int, Int, Double )
  val metrics_Grouped_By_App_Country: Future[ Map[ 
    ( Int, Option[ String ] ), 
//     - impressions: sum of impressions
//     - clicks: sum of clicks
//     - revenue: sum of revenue
    ( Int, Int, Double )
  ] ] = (
    for {
      impressions <- impressions_List
      rM <- revenue_Per_Impression_Map
    } yield impressions.foldLeft( 
      Map[ ( Int, Option[ String ] ), ( Int, Int, Double ) ]() ){
          case ( accum, Ad_Impression( app_id, advertiser_id, country_code, id ) ) => {
            //>val revenues: List[ Double ] = revenue_Per_Impression_Map
            val revenues: Seq[ Double ] = rM//revenue_Per_Impression_Map
              .getOrElse( id, Seq[ Double ]() )
            val revenue: Double = revenues.sum
            val clicks: Int = revenues.size
            val ( old_Im: Int, old_Cl: Int, old_Re: Double ) = accum
              .getOrElse( ( app_id, country_code ), ( 0, 0, 0D ) )
            
            accum.updated( 
              ( app_id, country_code ), 
              ( old_Im + 1, old_Cl + clicks, old_Re + revenue )
            ) 
            }
        }
  )
    .andThen {
      case _ => println( 
        ".andThen: `metrics_Grouped_By_App_Country` Future expected to be completed" )
    }
  metrics_Grouped_By_App_Country.foreach{ mGBAP =>
  println( 
    "metrics_Grouped_By_App_Country(" + 
    s"""${mGBAP.size}):\n""" + 
        mGBAP
          .take(7)
          .mkString("\n") + 
    "..." ) 
  }//?( executor = ExecutionContext.fromExecutor( Executors.newSingleThreadExecutor() ) )
  // this changes output order:
  val writing_Metrics: Future[ Map[ App_Country_Key, Fields_Sums_Value ] ] = 
    metrics_Grouped_By_App_Country
      .andThen{ case Success( mGBAP ) => {
        println( "About to write to `metrics.json` ..." )
        val mGBAP_Sorted = //metrics_Grouped_By_App_Country
          mGBAP
          .toSeq
          .sortBy( _._1 )
          .map{ case ( 
            ( app_id, country_code ), 
            ( impressions, clicks, revenue ) ) => 
              new StringBuilder( "{ \"app_id\": " )
                .addAll( "" + app_id )
                .addAll( ", \"country_code\": " )
                .addAll( country_code.map( v => "\""+ v + "\"" ).getOrElse("null") )
                .addAll( ", \"impressions\": " )
                .addAll( "" + impressions )
                .addAll( ", \"clicks\": " )
                .addAll( "" + clicks )
                .addAll( ", \"revenue\": " )
                .addAll( "" + revenue + " }" )
                .result()
          }
        println( "( ( app_id, country_code ), ( impressions, clicks, revenue ) )" )
        println( 
          s"""metrics_Grouped_By_App_Country(${mGBAP_Sorted.size}):\n""" + 
            mGBAP_Sorted
              .take( 11 )
              .mkString( "[\n", ",\n", ",\n..." ) + "\n" + mGBAP_Sorted.last + "\n]"
        ) 
      
        write_Text_Lines_To_File_With_FileChannel(
          "metrics.json",
          mGBAP_Sorted
      //       .init
      //       .+:("[")
      //       .:+(mGBAP_Sorted.last)
      //       .:+("]")
      //       .iterator
            .addString( new StringBuilder("[\n"), ",\n" )
            .addAll( "\n]" )
            .sliding( 80, 80 )
            .map( _.result() )
        )
        println( "Done ? ( with ) writing to `metrics.json`" )
      } 
    }
  
  val advertisers_Metrics_By_App_Country_Map: Future[ Map[ 
    ( Int, Option[ String ] ),
    Map[ Int, ( Int, Int , Double ) ]
  ] ] = ( 
    for {
      impressions <- impressions_List
      rM <- revenue_Per_Impression_Map
    } yield impressions.foldLeft( 
          Map[ 
            ( Int, Option[ String ] ), 
            Map[ Int, ( Int, Int , Double ) ]
          ]() 
        ){
          case ( 
            accum, 
            Ad_Impression( app_id, advertiser_id, country_code, id ) 
          ) => {
            val revenues: Seq[ Double ] = rM//revenue_Per_Impression_Map
              .getOrElse( id, Seq[ Double ]() )
            val revenue: Double = revenues.sum
            val clicks: Int = revenues.size
            // cases: ( update ) previous or ( add ) brand new
            val old_Map: Map[ Int, ( Int, Int , Double ) ] = 
              accum
                .getOrElse( 
                  ( app_id, country_code ), 
                  Map[ Int, ( Int, Int , Double ) ]() )
            val ( old_Im: Int, old_Cl: Int, old_Re: Double ) = old_Map
              .getOrElse( advertiser_id, ( 0, 0, 0D ) )
            // return
            accum.updated( 
              ( app_id, country_code ), 
              old_Map.updated( 
                advertiser_id, 
                ( old_Im + 1, old_Cl + clicks, old_Re + revenue )
              ) 
            ) 
          }
        } 
  )
    .andThen {
      case _ => println( 
        ".andThen: `advertisers_Metrics_By_App_Country_Map` Future expected to be completed" )
    }
  advertisers_Metrics_By_App_Country_Map.foreach{ aMBACM =>
  println( 
    "advertisers_Metrics_By_App_Country_Map(" + 
    s"""${aMBACM.size}):\n""" + 
        aMBACM
          .take(7)
          .mkString("\n") + 
    "..." ) 
  }//?( executor = ExecutionContext.fromExecutor( Executors.newSingleThreadExecutor() ) )
  // to bypass crazy type inference 
  type Row = ( Int, Option[ String ], List[ Int ] )
  val top_5_Advertisers_By_App_Country_List: Future[ List[ 
    Row
  ] ] = 
    advertisers_Metrics_By_App_Country_Map
      .map( _.foldLeft( List[ 
          Row
        ]() 
      ){
        case ( 
          accum: List[ Row ], 
          ( 
            ( app_id: Int, country_code: Option[ String ] ), 
            a_Map: Map[ _, _ ]//Int, ( Int, Int , Double ) ] 
          ) 
        ) => {
          // Map(17 -> (31,62,65.819239340359), 19 -> (15,0,0.0))
          val top_5_Advertisers: List[ Int ] = 
            a_Map
              .toList//toVector
              // It would fail on the following inputs: 
              // ((_ : Int), _), (_, (_, _, _)), (_, _)
              .sortBy{ case ( 
                  advertiser_id: Int, 
                  ( impressions: Int, clicks: Int, revenue: Double ) 
                ) => revenue * impressions / clicks
              }( 
                //Ordering[ Double ]
                Ordering.Double.TotalOrdering
                  .reverse 
              )
              .take(5)
              .map( _._1 )
          accum.::( 
            ( 
              app_id, country_code, top_5_Advertisers//List( 42 ) 
            ) 
          )
        }
      } )
      .andThen {
        case _ => println( 
          ".andThen: `top_5_Advertisers_By_App_Country_List` Future expected to be completed" )
      }
  top_5_Advertisers_By_App_Country_List
    .onComplete{
      case Success( t5ABACL ) => 
        println(
          "The `top_5_Advertisers_By_App_Country_List` succesfully completed with size: " + 
          t5ABACL.size )
      case Failure( t ) => println( "Could not ?process file?: " + t.getMessage )
    }
  
  val top_5_Advertisers: Future[ Seq[
    ( Int, Option[ String ], scala.collection.immutable.Iterable[ Int ] )
  ] ] = 
    advertisers_Metrics_By_App_Country_Map
      .map( _.groupMap( 
        item => ( item._1._1, item._1._2  ) )( 
        _._2
          .toList
          .sortBy( 
            element => element._2._3 * element._2._1 / element._2._2 
          )( Ordering.Double.TotalOrdering )
          .take(5)
          .map( _._1 ) 
      )
      .map( item => ( item._1._1, item._1._2, item._2.flatten ) )
      .toSeq )
      .andThen {
        case _ => println( 
          ".andThen: `top_5_Advertisers` Future expected to be completed" )
      }
  top_5_Advertisers.onComplete{
    case Success(t5A) => println("The `top_5_Advertisers` succesfully completed with size: " + t5A.size)
    case Failure(t) => println("Could not ?process file?: " + t.getMessage)
  }
  val triplet_Ordering = 
    Ordering
      .by[ ( Int, ( Int, Option[ String ] ) ), Int ]( _._1 )
      .reverse
      .orElseBy[ Int ]( _._2._1 )
      .orElseBy[ Option[ String ] ]( _._2._2 )
// top_5_Advertisers(80):
  top_5_Advertisers.foreach{ t5A =>
  println( 
    "top_5_Advertisers(" + 
    s"""${t5A.size}):\n""" + 
        t5A
          .sortBy( v => ( v._3.size, ( v._1, v._2 ) ) )( 
            triplet_Ordering
          )
          .take(11)
          .mkString("\n") + 
    "..." ) }//?( executor = ExecutionContext.fromExecutor( Executors.newSingleThreadExecutor() ) )
  // (10,Some(DE),List(14, 16))
  top_5_Advertisers_By_App_Country_List.foreach{ t5ABACL =>
  println( 
    "top_5_Advertisers_By_App_Country_List(" + 
    s"""${t5ABACL.size}):\n""" + 
        t5ABACL
          .sortBy( v => ( v._3.size, ( v._1, v._2 ) ) )( triplet_Ordering )
          .take(11)
          .mkString("\n") + 
    "..." ) 
  }//?( executor = ExecutionContext.fromExecutor( Executors.newSingleThreadExecutor() ) )
  
  val write_Recommendation: Future[ Unit ] = 
    for {
      t5ABACL <- top_5_Advertisers_By_App_Country_List
      mGBAP <- metrics_Grouped_By_App_Country
    } yield {
      println( "About to write to `recommendation.json` ..." )
      write_Text_Lines_To_File_With_FileChannel(
        "recommendation.json",
          t5ABACL//top_5_Advertisers_By_App_Country_List
            .zipWithIndex
            .map{ case ( 
              ( app_id, country_code, list ), i ) => {
                val prefix = if( i > 0 ){ "{" }else{ "[\n{" }
                val suffix = if( 
                  i < mGBAP.size - 1//metrics_Grouped_By_App_Country.size - 1 
                ){ 
                  "},\n" }else{ "}\n]" }
                val code = if( country_code.isEmpty ){ 
                  "null" 
                }else{ 
                  "\"" + country_code.get + "\""
                }
                
                prefix + 
                s""""app_id": ${app_id}""" +
                s""", "country_code": ${code}""" + 
                s""", "recommended_advertiser_ids": ${list.mkString("[", ", ", "]")}""" +
                suffix
                }
            }
            .iterator
      )
      println( "Done ? ( with ) writing to `recommendation.json`" )
    }
  
  /** */
  def stop_ExecutorService( 
    executorService: java.util.concurrent.ExecutorService = executorService, 
    // TimeUnit
    delay: Long = 1000L
  ): Unit = {
    println( "About to shutdown `executorService` ..." )
    executorService.shutdown()
    try {
      if ( 
        !executorService
          .awaitTermination( delay, TimeUnit.MILLISECONDS ) 
      ) {
        println( "not awaitTermination => About to shutdownNow `executorService` ..." )
        executorService.shutdownNow()
          .forEach( println( _ ) )
      }else{
        println( "executorService.awaitTermination" )
      }
    } catch { case e: InterruptedException =>
        println( "catch -> InterruptedException" )
        executorService
          .shutdownNow()
          .forEach( println( _ ) )
    } finally {
      println( "finally after trying to shutdown `executorService`" )
    }
  }
  
  Future
    .sequence( 
      Seq(
//         clicks_List,
//         revenue_Per_Impression_Map,
//         impressions_List,
//         metrics_Grouped_By_App_Country,
//         advertisers_Metrics_By_App_Country_Map,
        top_5_Advertisers, 
        top_5_Advertisers_By_App_Country_List, 
        writing_Metrics,
        write_Recommendation
      )
    )
    .onComplete{ _ => {
        println( "Future.sequence( ... ).onComplete ..." )
        println( s"ExecutionContextExecutorService.isShutdown(): ${ec.isShutdown()}" )
        println( s"ExecutionContextExecutorService.isTerminated(): ${ec.isTerminated()}" )
        println( "About to shutdownNow ExecutionContextExecutorService ..." )
        //executionContext
//         ec
//           .shutdownNow() 
//           .forEach( println( _ ) )
        stop_ExecutorService()
        println( s"ExecutionContextExecutorService.isShutdown(): ${ec.isShutdown()}" )
        println( s"ExecutionContextExecutorService.isTerminated(): ${ec.isTerminated()}" )
      }
    }
}

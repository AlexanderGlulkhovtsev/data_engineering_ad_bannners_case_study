package data_mining

import scala.util.Using
//import scala.io.BufferedSource

//?!import com.fortysevendeg.lambdatest._

//?import org.scalacheck._

import org.scalatest.{
  FunSuite,
  DiagrammedAssertions,
  Informer,
  GivenWhenThen
}
import org.scalatest.Assertions._

//import java.util.Base64
//import java.util.Base64.Decoder

// import ujson.Js
// 
// import JSON_Parser.{ 
//   get_Array_Items_Iter,
//   get_Object_KV_Pairs_Iter,
//   get_Value
// }
import Naive_Approach.{ 
  Ad_Event, Ad_Click, Ad_Impression, 
  extract_Paths_From_Args 
}
import JSON_Parser.{
  ad_Clicks_Parser,
  ad_Impressions_Parser
}
import Files_Reader.{
  read_Chars_From_File, 
  get_Files_Content
}


/** 
> show test:definedTestNames 
> testOnly data_mining.Test__Files_Reader
*/
class Test__Files_Reader
  extends FunSuite
  ///! it gives a hell of an output !
  //with DiagrammedAssertions
  with GivenWhenThen 
{

  //val test_Input_1_Path_Name = "./src/test/resources/test_input_1.json"
  test(
    "read_Chars_From_File"
  ) {
    val file_Path_Name: String = "./src/test/resources/clicks_compact_lines.json" 
    val actual_Result = read_Chars_From_File( file_Path_Name )
    
    assert( actual_Result.nonEmpty, "reader expected to be nonEmpty" )
    assertResult( 8246 ) { actual_Result.size }
    //?assertResult( '[' ) { actual_Result.take(1) }
    assertResult( '[' ) { actual_Result.head }
    assertResult( 
      Array( '[', '\n', '{', '"', 'i', 'm', 'p' ) 
    ) { actual_Result.take(7) }
  }

  test("Read from source files with get_Files_Content and ad_Clicks_Parser") {
    // Informer(s):
    Given("a list of click events file names with paths")
    //When("an element is added")
    //Then("the Set should have size 1")
    And("the ad_Clicks_Parser as content_Parser")
    val actual_Result: Iterable[ Ad_Event ] = get_Files_Content(
      Seq(
        // 1: 95 events
        "./src/test/resources/clicks_compact_lines.json",
        // 2: 95 events
        "./src/test/resources/clicks_one_liner.json",
        // 3: 95 events
        "./src/test/resources/clicks_4.json",
        // 4: 95 events
        "./src/test/resources/clicks_3.json",
        // 5: 95 events
        "./src/test/resources/clicks_2.json",
        // 6: 95 events
        "./src/test/resources/clicks_1.json",
        // 7: 95 events
        "./src/test/resources/clicks.json"
      ),
      content_Parser = ad_Clicks_Parser
    )
    assert( actual_Result.nonEmpty, "Files_Content expected to be nonEmpty" )
    /// @Done: @fixEd: Ad_Event and Char are unrelated
    assert( 
      actual_Result.head == Ad_Click( 
        "97dd2a0f-6d42-4c63-8cd6-5270c19f20d6", 2.091225600111518 
      ), "Files_Content first row item expected to be Ad_Click event ? content ?" 
    )
    //val chars_Count: Int = 71395
    // rows of events items 
    val expected_Records: Int = 665 // 7 * 95 = 665
    //>withClue( "this is a clue:\n" + actual_Result.mkString ) {
      assert( actual_Result.size == expected_Records )
    //}
    assert( 
      actual_Result.last == Ad_Click( 
        "b2a60f0d-1d08-4738-9dc5-0eed9cddee82", 0.80450874493205
      ), "Files_Content last row item expected to be Ad_Click event ? content ?" 
    )
    
//     withClue("this is a clue") {
//       assertThrows[IndexOutOfBoundsException] {
//         "hi".charAt(-1)
//       }
//     }
    info("That's all folks!")
  }

  test("Read from source files with get_Files_Content and ad_Impressions_Parser") {
    // Informer(s):
    Given("a list of click events file names with paths")
    //When("an element is added")
    //Then("the Set should have size 1")
    And("the ad_Impressions_Parser as content_Parser")
    val actual_Result: Iterable[ Ad_Event ] = get_Files_Content(
      Seq(
        // 1: 7 events
        "./src/test/resources/impressions_4.json",
        // 2: 7 events
        "./src/test/resources/impressions_3.json",
        // 3: 7 events
        "./src/test/resources/impressions_2.json",
        // 4: 7 events
        "./src/test/resources/impressions_1.json",
// >>> from json import ( load )
// >>> with open( "impressions.json" ) as f:imp = load( f  );print( len( imp ), imp[0] );
// ... 
// 2047 {'advertiser_id': 8, 'country_code': 'UK', 'app_id': 32, 'id': 'a39747e8-9c58-41db-8f9f-27963bc248b5'}
        // 5: 2047 events
        "./src/test/resources/impressions.json"
      ),
      content_Parser = ad_Impressions_Parser
    )
    assert( actual_Result.nonEmpty, "Files_Content expected to be nonEmpty" )
    assert( 
      actual_Result.head == Ad_Impression( 
        32, 8, Some( "UK" ), "a39747e8-9c58-41db-8f9f-27963bc248b5"
      ), "Files_Content first row item expected to be Ad_Impression event ? content ?" 
    )
    // rows of events items 
    val expected_Records: Int = 2075 // 7 * 4 + 2047 = 2075
      assert( actual_Result.size == expected_Records )
    assert( 
      actual_Result.last == Ad_Impression( 
        /// ? out of order ? not last from impressions.json ? but from impressions_[1..4].json ?
        //?15, 27, None, "81cf2616-4fb5-4c47-8517-00dbea47984d"
        5, 12 ,None, "7d5488b6-db81-4a5e-87a0-6828bf75ec0a"
      ), "Files_Content last row item expected to be Ad_Impression event ? content ?" 
    )
    
    info("That's all folks!")
  }
  
  test("extract_Paths_From_Args") {
    val actual_Result = extract_Paths_From_Args( Array( 
      "./src/test/resources/clicks_2.json", 
      "./src/test/resources/impressions_3.json" 
    ) )
    val expected_Result = Map(
      "clicks" -> List( "./src/test/resources/clicks_2.json" ),
      "impressions" -> List( "./src/test/resources/impressions_3.json" ) 
    )
    assert( actual_Result.size == 2 )
    assert( actual_Result == expected_Result )
  }
  
  ignore("Read from source files") {
    val main_Args: String = """
      ./src/test/resources/impressions_4.json ./src/test/resources/impressions_3.json ./src/test/resources/impressions_2.json ./src/test/resources/impressions_1.json ./src/test/resources/impressions.json ./src/test/resources/clicks_compact_lines.json ./src/test/resources/clicks_one_liner.json ./src/test/resources/clicks_4.json ./src/test/resources/clicks_3.json ./src/test/resources/clicks_2.json ./src/test/resources/clicks_1.json ./src/test/resources/clicks.json
    """
    assert( false )
  }

}

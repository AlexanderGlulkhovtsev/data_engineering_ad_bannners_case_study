package data_mining

import scala.util.Using
//import scala.io.BufferedSource

//?!import com.fortysevendeg.lambdatest._

//?import org.scalacheck._

import org.scalatest.{
  FunSuite,
  DiagrammedAssertions
}
import org.scalatest.Assertions._

//import java.util.Base64
//import java.util.Base64.Decoder

import ujson.Js

import JSON_Parser.{ 
  get_Array_Items_Iter,
  get_Object_KV_Pairs_Iter,
  get_Value,
  JSON_Scalar, JSON_String, JSON_Integer, JSON_Float, JSON_Null,
  events_Parser,
  ad_Clicks_Parser,
  ad_Impressions_Parser
}
import Naive_Approach.{ 
  Ad_Event, Ad_Click, Ad_Impression, 
}


/** 
> show test:definedTestNames 
[info] * data_mining.Test_JSON_Parser
> testOnly data_mining.Test_JSON_Parser
*/
class Test_JSON_Parser 
  extends FunSuite//LambdaTest 
  with DiagrammedAssertions
{

  test( "Ad_Event" ) {
    assertResult( 
      Ad_Click( "b2a60f0d-1d08-4738-9dc5-0eed9cddee82", 0.80450874493205 ), 
      "this is a clue" 
    ) { 
      Ad_Click
        .from_Seq( 
          Seq( "b2a60f0d-1d08-4738-9dc5-0eed9cddee82", 0.80450874493205 ) 
        )
    }
    assert( 
      Ad_Impression.from_Seq( 
        Seq( 32, 8, Some( "UK" ), "a39747e8-9c58-41db-8f9f-27963bc248b5" )
      ) == Ad_Impression( 
        32, 8, Some( "UK" ), "a39747e8-9c58-41db-8f9f-27963bc248b5"
      ), 
      "expected to be the same" 
    )
    assert( 
      Ad_Impression.from_Seq( 
        Seq( 
          30, 17, None, "5deacf2d-833a-4549-a398-20a0abeec0bc"
        )
      ) == Ad_Impression( 
        30, 17, None, "5deacf2d-833a-4549-a398-20a0abeec0bc"
      ), 
      "expected to be the same" 
    )
  }
  
  test( "convert `metrics_Grouped_By_App_Country` to JSON" ) {
    // country_codes: "DE", "US", "IT", null
    // ( Grouped_By ( app_id, country_code ), sum of ( impressions, clicks, revenue ) )
    // [ ( Int, Option[ String ] ), ( Int, Int, Double ) ]
    val metrics_Grouped_By_App_Country: Seq[ 
      ( ( Int, Option[ String ] ), ( Int, Int, Double ) ) ] = 
      Seq(
        ( ( 0, Some( "DE" ) ), ( 11, 33, 9999.555 ) ),
        ( ( 36, Some( "US" ) ), ( 77, 113, 6666.66 ) ),
        ( ( 4, Some( "IT" ) ), ( 333, 227, 42.13 ) ),
        ( ( 40, None ), ( 1342, 4213, 987654321.123456789 ) ),
        ( ( 11, Some( "US" ) ), ( 512, 313, 13.42 ) ),
      )
    val mGBAP_Sorted_Strings = metrics_Grouped_By_App_Country
      //.toSeq
      .sortBy( _._1 )
      .map{ case ( 
        ( app_id, country_code ), 
        ( impressions, clicks, revenue ) ) => 
          s"""{"app_id": ${app_id}""" +
          s""", "country_code": ${country_code.map( v => s""""${v}"""" ).getOrElse("null")}""" + 
          s""", "impressions": $impressions, "clicks": $clicks, "revenue": $revenue}"""
      }
    val actual_Result = mGBAP_Sorted_Strings
      .init
      .map( _ + "," )
      .+:("[")
      .:+( mGBAP_Sorted_Strings.last )
      .:+("]")
      //?.iterator
    val expected_Result = Seq(//?Iterator(
      "[",
      """{"app_id": 0, "country_code": "DE", "impressions": 11, "clicks": 33, "revenue": 9999.555},""",
      """{"app_id": 4, "country_code": "IT", "impressions": 333, "clicks": 227, "revenue": 42.13},""",
      """{"app_id": 11, "country_code": "US", "impressions": 512, "clicks": 313, "revenue": 13.42},""",
      """{"app_id": 36, "country_code": "US", "impressions": 77, "clicks": 113, "revenue": 6666.66},""",
      """{"app_id": 40, "country_code": null, "impressions": 1342, "clicks": 4213, "revenue": 9.876543211234568E8}""",
      "]",
    )
    assert( actual_Result.size == expected_Result.size, "expected to be equal" )
//     assertResult( expected_Result, "this is a clue" ) { 
//       actual_Result
//     }
    assert( 
      actual_Result == expected_Result, 
      "expected to be equal\n" + 
      actual_Result
        .zip( expected_Result )
        .map{ case ( a_L, e_L ) => if( a_L == e_L ){ s"${a_L} == ${e_L}" }else{
          e_L + "\n" + a_L + 
          a_L.zip( e_L )
            //.foldLeft( ( diff, a, e ) )
            .foldLeft( "" ){ case ( diff, ( a_C, e_C ) ) => 
              if( a_C == e_C ){ diff + " " }else{ diff + "^" } } } }
        .mkString("\n")
    )
    
    val expected_Result_0 = Seq(
      "[\n" + """{"app_id": 0, "country_code": "DE", "impressions": 11, "clicks": 33, "revenue": 9999.555},""",
      """{"app_id": 4, "country_code": "IT", "impressions": 333, "clicks": 227, "revenue": 42.13},""",
      """{"app_id": 11, "country_code": "US", "impressions": 512, "clicks": 313, "revenue": 13.42},""",
      """{"app_id": 36, "country_code": "US", "impressions": 77, "clicks": 113, "revenue": 6666.66},""",
      """{"app_id": 40, "country_code": null, "impressions": 1342, "clicks": 4213, "revenue": 9.876543211234568E8}""" + "\n]"
    )
    val actual_Result_0 = mGBAP_Sorted_Strings
      .zipWithIndex
      .map{ case ( json_Obj_Str, i ) => {
          val prefix = if( i > 0 ){ "" }else{ "[\n" }
          val suffix = if( 
            i < mGBAP_Sorted_Strings.size - 1 
          ){ 
            "," }else{ "\n]" }
            
          prefix + json_Obj_Str + suffix
        }
      }
      //?.iterator
    assert( 
      //?actual_Result_0 == expected_Result_0, 
      actual_Result_0.mkString == expected_Result_0.mkString, 
      "unexpected result\n" + actual_Result_0.mkString("\n") )
      
    val actual_Result_2 = mGBAP_Sorted_Strings
      .drop(1)
      .init
      .map( _ + "," )
      .+:( mGBAP_Sorted_Strings.headOption.map( "[\n" + _ + "," ).getOrElse("[") )
      .:+( mGBAP_Sorted_Strings.lastOption.map( "" + _  + "\n]" ).getOrElse("]") )
    assert( 
      actual_Result_2 == expected_Result_0, 
      "unexpected result\n" + actual_Result_2.mkString("\n") )
      
    val actual_Result_3 = mGBAP_Sorted_Strings
      .addString( new StringBuilder("[\n"), ",\n" )
      .addAll("\n]")
      .mkString
    assert( 
      actual_Result_3 == expected_Result_0.mkString("\n"), 
      "unexpected result\n" + actual_Result_3 )
    
    val actual_Result_4 = metrics_Grouped_By_App_Country
      .map{ case ( 
        ( app_id, country_code ), 
        ( impressions, clicks, revenue ) ) => 
          new StringBuilder( "{ \"app_id\": " )
            .addAll( "" + app_id )
            .addAll( ", \"country_code\": " )
            .addAll( country_code.map( v => "\""+ v + "\"" ).getOrElse("null") )
            .addAll( ", \"impressions\": " )
            .addAll( "" + impressions )
            .addAll( ", \"clicks\": " )
            .addAll( "" + clicks )
            .addAll( ", \"revenue\": " )
            .addAll( "" + revenue + " }" )
            .result()
      }
      .addString( new StringBuilder("[\n"), ",\n" )
      .addAll( "\n]" )
      .sliding( 80, 80 )
      //.mkString
    val expected_Result_4 = 
"""[
{ "app_id": 0, "country_code": "DE", "impressions": 11, "clicks": 33, "revenue": 9999.555 },
{ "app_id": 36, "country_code": "US", "impressions": 77, "clicks": 113, "revenue": 6666.66 },
{ "app_id": 4, "country_code": "IT", "impressions": 333, "clicks": 227, "revenue": 42.13 },
{ "app_id": 40, "country_code": null, "impressions": 1342, "clicks": 4213, "revenue": 9.876543211234568E8 },
{ "app_id": 11, "country_code": "US", "impressions": 512, "clicks": 313, "revenue": 13.42 }
]"""
    assert( 
      actual_Result_4.mkString == ( expected_Result_4
      ),//?.toSeq.sliding( 80, 80 ).map( _.unwrap ), 
      "unexpected result\n" + actual_Result_4.mkString )
    
    val actual_Result_5 = metrics_Grouped_By_App_Country
      .foldLeft( scala.collection.mutable.ArrayBuffer("[\n") ){ 
        case ( ab, ( 
        ( app_id, country_code ), 
        ( impressions, clicks, revenue ) ) ) => 
          ab.addOne(
            new StringBuilder( "{ \"app_id\": " )
              .addAll( "" + app_id )
              .addAll( ", \"country_code\": " )
              .addAll( country_code.map( v => "\""+ v + "\"" ).getOrElse("null") )
              .addAll( ", \"impressions\": " )
              .addAll( "" + impressions )
              .addAll( ", \"clicks\": " )
              .addAll( "" + clicks )
              .addAll( ", \"revenue\": " )
              .addAll( "" + revenue + " }" )
              .addAll( 
                if( ab.size >= metrics_Grouped_By_App_Country.size ){ 
                "" }else{ ",\n" } ) 
              .result( )
          ) 
      }
      .addOne( "\n]" )
    
    assert( 
      actual_Result_5.mkString == expected_Result_4,
      "unexpected result\n" + actual_Result_5.mkString )
  }
  
  test( "JSON_Scalar get_Value" ) {
    assertResult( "97dd2a0f-6d42-4c63-8cd6-5270c19f20d6", "this is a clue" ) { 
      get_Value( "\"97dd2a0f-6d42-4c63-8cd6-5270c19f20d6\"" ).value
    }
    assert( get_Value("\"UK\"").value == "UK", "expected to be String" )
    assert( get_Value("5").value == 5, "expected to be Integer" )
    assert( get_Value("12").value == 12, "expected to be Integer" )
    assert( get_Value("2.091225600111518").value == 2.091225600111518, "expected to be Double" )
    assert( get_Value("null").value == None, "expected to be None" )
    assert( get_Value("").value == None, "expected to be None" )
  }
  
  test( "get_Object_KV_Pairs_Iter" ) {
    val ( kV_Pairs_Iter, rest_Iter ) = get_Object_KV_Pairs_Iter( 
      input = """"impression_id":"97dd2a0f-6d42-4c63-8cd6-5270c19f20d6","revenue":2.091225600111518"""
        .iterator 
    )
    assert(
      kV_Pairs_Iter.next() == ( "impression_id", "\"97dd2a0f-6d42-4c63-8cd6-5270c19f20d6\"" ), 
      "expected to be equal" )
    assert(
      kV_Pairs_Iter.next() == ( "revenue", "2.091225600111518" ), 
      "expected to be equal" )
      
    val ( kVs_Iter, _ ) = get_Object_KV_Pairs_Iter( 
      input = """
        "app_id": 30,
        "advertiser_id": 17,
        "country_code": null,
        "id": "5deacf2d-833a-4549-a398-20a0abeec0bc"
    """
        .iterator 
    )
    assert(
      kVs_Iter.next() == ( "app_id", "30" ), 
      "expected to be equal" )
    assert(
      kVs_Iter.next() == ( "advertiser_id", "17" ), 
      "expected to be equal" )
    assert(
      kVs_Iter.next() == ( "country_code", "null" ), 
      "expected to be equal" )
    assert(
      kVs_Iter.next() == ( "id", "\"5deacf2d-833a-4549-a398-20a0abeec0bc\"" ), 
      "expected to be equal" )
  }
  
  //ignore(
  test(
    "get_Array_Items_Iter"
  ) {
    // 
    Using.resource(
      scala.io.Source.fromFile( 
        "./src/test/resources/clicks_compact_lines.json", "UTF8" )
    ){ reader => {
        assert( reader.nonEmpty, "reader expected to be nonEmpty" )
        val source_Content: Seq[ Char ] = reader.toSeq
        val actual_Result = get_Array_Items_Iter( 
          input = source_Content.iterator, 
          is_DeBug_Mode = 1 == 0
        )
        val ( array_Items_Iter, rest_Iter ) = actual_Result
        
        assertResult( 
          //Some(']'), 
          """"impression_id":"97dd2a0f-6d42-4c63-8cd6-5270c19f20d6","revenue":2.091225600111518""",
          "this is a clue" 
        ) { 
          array_Items_Iter
            .next()
            .mkString
            //.toSeq
            //.headOption 
        }
        assert( 
          array_Items_Iter.drop(93).next().mkString == """"impression_id":"b2a60f0d-1d08-4738-9dc5-0eed9cddee82","revenue":0.80450874493205""", 
          "expected to be ???" )
        //?assert( rest_Iter.isEmpty, "rest_Iter expected to be Empty" )
        assert( rest_Iter.toSeq == Seq('\n'), "expected to be ???" )
      }
    }
  
  }

  //ignore(
  test(
    "ad_Clicks_Parser"
  ) {
    Using.resource(
      scala.io.Source.fromFile( 
        "./src/test/resources/clicks_compact_lines.json", "UTF8" )
    ){ reader => {
        val actual_Result: Iterator[ Ad_Event ] = ad_Clicks_Parser( input = reader )
        
        assert( actual_Result.nonEmpty, "expected to be nonEmpty" )
        assert( actual_Result.next() == Ad_Click( 
          "97dd2a0f-6d42-4c63-8cd6-5270c19f20d6", 
          2.091225600111518 ), 
          "expected to be the same" )
        assert( 
          actual_Result
            .drop(93)
            .next() == Ad_Click( 
            "b2a60f0d-1d08-4738-9dc5-0eed9cddee82", 0.80450874493205
          ), "expected to be the same" )
      }
    }
  }
  
  //ignore(
  test(
    "ad_Impressions_Parser"
  ) {
    Using.resource(
      scala.io.Source.fromFile( 
        "./src/test/resources/impressions.json", "UTF8" )
    ){ reader => {
        val actual_Result: Iterator[ Ad_Event ] = ad_Impressions_Parser( input = reader )
        
        assert( actual_Result.nonEmpty, "expected to be nonEmpty" )
        assert( actual_Result.next() == Ad_Impression( 
            32, 8, Some( "UK" ), "a39747e8-9c58-41db-8f9f-27963bc248b5"
          ), 
          "expected to be the same" )
        assert( actual_Result.next() == Ad_Impression( 
            30, 17, None, "5deacf2d-833a-4549-a398-20a0abeec0bc"
          ), 
          "expected to be the same" )
        assert( actual_Result.drop(2).next() == Ad_Impression( 
            9, 32, None, "b15449b6-14c9-406b-bce9-749805dd6a3e"
          ), 
          "expected to be the same" )
        assert( 
          actual_Result
            .drop(2041)
            .next() == Ad_Impression( 
            // same value over and over again !
            //40, 14, Some("DE"), "fb1af15e-fb01-43c7-bbaf-bcb575ff3832"
            15, 27, None, "81cf2616-4fb5-4c47-8517-00dbea47984d"
          ), "expected to be the same" )
      }
    }
  }
  
  ignore(
  //test(
    "Parse source file with custom parser"
  ) {
    // 
    Using.resource(
      scala.io.Source.fromFile( 
        "./src/test/resources/clicks_compact_lines.json", "UTF8" )
    ){ reader => {
        assert( reader.nonEmpty, "reader expected to be nonEmpty" )
        val actual_Result = ???
        
        assert( false )
      }
    }
  }
  
  test(
    "Parse source file with ujson"
  ) {
    Using.resource(
      scala.io.Source.fromFile( "./src/test/resources/clicks_compact_lines.json", "UTF8" )
    ){ reader =>
      //val actual_Result = 
      //! required: ujson.Readable
      val json = ujson.read( 
        reader
          //!.bufferedReader() // : java.io.BufferedReader
          //!.toArray // : Array[Char]
          //!.reader() // : java.io.InputStreamReader
          .mkString
      )
      assert( json(0)("revenue").num == 2.091225600111518 )
      assert( json.arr.size == 95 )
      assert( json(94)("impression_id").str == "b2a60f0d-1d08-4738-9dc5-0eed9cddee82" )
    }
  }
  
  test(
    "Transform data to JSON string with ujson"
  ) {
    val json_Ast = ujson.Arr( 1, 2, 3 )
    val json_Str = 
      ujson
        .transform(
          //upickle.default.transform(Seq(1, 2, 3)),
          json_Ast,
          // ujson.StringRenderer(indent: Int, escapeUnicode: Boolean)
          ujson.StringRenderer()
        ).toString
    assert( json_Str == "[1,2,3]" )
    val expected_Result: String = """[{"app_id":0,"country_code":null,"impressions":4,"clicks":0,"revenue":0},{"app_id":0,"country_code":"IT","impressions":8,"clicks":0,"revenue":0},{"app_id":1,"country_code":"IT","impressions":21,"clicks":21,"revenue":48.09063975343832}]"""
    val actual_Result = 
      ujson
        .transform(
          ujson
          // mutable.LinkedHashMap[String,ujson.Value])
          // or items: (String, ujson.Value)*
          .Arr(
          //?.Arr.from(//! No implicit view available from Seq[(String, Any)] => ujson.Value
            Seq(
              ujson.Obj(
                "app_id" -> ujson.Num(0), 
                "country_code" -> ujson.Null,
                "impressions" -> ujson.Num(4), 
                "clicks" -> ujson.Num(0), 
                "revenue" -> ujson.Num(0.0) 
              ),
              ujson.Obj(
                "app_id" -> 0, 
                "country_code" -> "IT",
                "impressions" -> 8, 
                "clicks" -> 0, 
                "revenue"-> 0.0
              ),
              ujson.Obj(
                "app_id" -> ujson.Num(1), 
                "country_code" -> ujson.Str( "IT" ),
                "impressions" -> ujson.Num(21), 
                "clicks" -> ujson.Num(21), 
                "revenue" -> ujson.Num(48.09063975343832) 
              ) 
            ): _*/*
              Seq( 
                ( "app_id", 0 ), 
                ( "country_code", null ), 
                ( "impressions", 4 ), 
                ( "clicks", 0 ), 
                ( "revenue", 0.0 )
              ),
              Seq( 
                ( "app_id", 0 ), 
                ( "country_code", "IT" ), 
                ( "impressions", 8 ), 
                ( "clicks", 0 ), 
                ( "revenue", 0.0 )
              ),
              Seq( 
                ( "app_id", 1 ), 
                ( "country_code", "IT" ), 
                ( "impressions", 21 ), 
                ( "clicks", 21 ), 
                ( "revenue", 48.09063975343832 )
              )
            )
            //?.map( ujson.Obj( _ ) ): _*
            //?.map( o => ujson.Obj( o ) ): _*
            //?.map( v => ujson.Value( v ) ): _* // required: ujson.Readable
            //?.map{ case v: Seq[(String, Any)] => ujson.Value.JsonableDict( v ) }: _*
            .map{ case Seq( 
              ( a, aV: Int ), 
              ( cc, ccV ), 
              ( i, iV: Int ), 
              ( c, cV: Int ), 
              ( r, rV: Double ) 
            ) =>
              ujson.Obj( 
                a -> ujson.Num(aV), 
                cc -> (if( cc == null ){ ujson.Null }else{ ujson.Str( ccV.toString ) }), 
                i -> ujson.Num(iV), 
                c -> ujson.Num(cV), 
                r -> ujson.Num(rV) ) }: _**/
          ),
          ujson.StringRenderer()
        ).toString
    assert( actual_Result == expected_Result )
  }
  
  //test
  ignore("An empty Set should have size 0") {
    assert(Set.empty.size == 0)
    withClue("this is a clue") {
      assertThrows[IndexOutOfBoundsException] {
        "hi".charAt(-1)
      }
    }
  }

}

// object Test_JSON_Parser extends App {
//   run( "Test-JSON-Parser", new Test_JSON_Parser )
// }
